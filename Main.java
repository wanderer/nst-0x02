package telnet;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;



public class Main {

	public static void main(String[] args) {
		String host = "smtp.utb.cz";
		int port = 25;

		if (args.length>0) {
			host=args[0];
		}
		if(args.length>1) {
			port=Integer.parseInt(args[1]);
		}
		try {
			Socket s = new Socket(host, port);
			InputStream is = s.getInputStream();
			OutputStream os = s.getOutputStream();
			while(true) {
				int c = is.available();
				if(c>0) {
					int ch = is.read();
					if(ch==-1)
					{
						System.out.println("* we're done here");
						break;
					}
					System.out.write(ch);
					}
				c = System.in.available();
					if(c>0){
						os.write(System.in.read());
					}
				}
			}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
